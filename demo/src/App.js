import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Counter from "./components/Counter/Counter";

import SubscriptionsList from "./components/SubscriptionsList/SubscriptionsList";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Counter />
        <SubscriptionsList />
      </header>
    </div>
  );
}

export default App;
