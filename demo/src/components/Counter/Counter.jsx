import React from "react";
import { connect } from "react-redux";
import { increaseCount } from "../../redux/actions";

const Counter = ({ count, increaseCount, decreaseCount }) => {
  return (
    <>
      <p>{count}</p>
      <button onClick={increaseCount}>Increase</button>
      <button onClick={decreaseCount}>Decrease</button>
    </>
  );
};

Counter.defaultProps = {
  count: 2,
  increaseCount: () => console.log("failed to increase: no callback prop"),
  decreaseCount: () => console.log("failed to decrease: no callback prop")
};

const mapStateToProps = state => {
  return { count: state.count };
};

const mapDispatchToProps = dispatch => {
  return {
    increaseCount: () => {
      const action = increaseCount(1);
      dispatch(action);
    },
    decreaseCount: () => {
      const action = increaseCount(-1);
      dispatch(action);
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
