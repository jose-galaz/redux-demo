import React from "react";
import { connect } from "react-redux";
import { addSubscription } from "../../redux/actions";

const SubscriptionsList = ({ subscriptions, addGroup }) => (
  <>
    <button onClick={() => addGroup(`Group ${subscriptions.length}`)}>
      Add group
    </button>
    <ul>
      {subscriptions.length === 0
        ? "No subscriptions"
        : subscriptions.map(groupName => <li key={groupName}>{groupName}</li>)}
    </ul>

  </>
);

SubscriptionsList.defaultProps = {
  subscriptions: ["grupo1", "grupo2"],
  addGroup: name => console.log("Add group failed: no callback prop")
};

const mapStateToProps = state => {
  return { subscriptions: state.subscriptions };
};

const mapDispatchToProps = dispatch => {
  return {
    addGroup: name => {
      const action = addSubscription(name);
      dispatch(action);
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionsList);
