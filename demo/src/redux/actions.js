import { INCREASE_COUNT } from "./actionTypes";
import { ADD_SUBSCRIPTION } from "./actionTypes";

export const increaseCount = amount => ({
  type: INCREASE_COUNT,
  amount
});

export const addSubscription = groupName => ({
  type: ADD_SUBSCRIPTION,
  groupName
});
