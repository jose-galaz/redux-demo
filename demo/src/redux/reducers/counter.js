import { INCREASE_COUNT } from "../actionTypes";

export default function(state = 1, action) {
  switch (action.type) {
    case INCREASE_COUNT: {
      const count = state + action.amount;
      return count;
    }
    default:
      return state;
  }
}
