import { combineReducers } from "redux";
import count from "./counter.js";
import subscriptions from "./subscriptions.js";

export default combineReducers({
  count,
  subscriptions
});
