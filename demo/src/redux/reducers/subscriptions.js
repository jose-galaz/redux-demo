import { ADD_SUBSCRIPTION } from "../actionTypes";

export default function(state = [], action) {
  switch (action.type) {
    case ADD_SUBSCRIPTION: {
      return [...state, action.groupName];
    }
    default:
      return state;
  }
}
